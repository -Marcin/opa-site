<?php

class WydarzeniaUsluga {
    private $wydarzeniaDao;
    private $miejsceWydarzeniaDao;
    
    public function __construct() {
        $this->wydarzeniaDao = new WydarzenieDao();
        $this->miejsceWydarzeniaDao = new MiejsceWydarzeniaDao();
    }
    
    
    public function zapiszWydarzenie($wydarzenie) {
        if (!$wydarzenie->miejsceWydarzenia->id > 0) {
            $wydarzenie->miejsceWydarzenia->id = $this->miejsceWydarzeniaDao->zapiszMiejsce($wydarzenie->miejsceWydarzenia);
        }
        return $this->wydarzeniaDao->zapiszWydarzenie($wydarzenie);
    }
    
    public function pobierzNajblizszeWydarzenia($ilosc) {
        $kryteria = new KryteriaWyszukiwaniaWydarzen();
        $kryteria->ilosc = $ilosc;
        $kryteria->dataOd = time();
        $kryteria->orderBy = "dataczas ASC";
        return $this->wydarzeniaDao->pobierzListeWydarzen($kryteria);
    }
    
    public function pobierzWydarzeniaMiesiaca($miesiac) {
        $kryteria = new KryteriaWyszukiwaniaWydarzen();
//        $kryteria->miesiac = $miesiac;
        return $this->wydarzeniaDao->pobierzListeWydarzen($kryteria);
    }
    
    public function generujJS($wydarzenia) {
        $js = "";
        foreach($wydarzenia as $wydarzenie) {
            $data = date('Y-m-d', $wydarzenie->dataCzas);
            $godzina = date('H:i', $wydarzenie->dataCzas);
            $nazwa = $wydarzenie->miejsceWydarzenia->nazwa;
            $miasto = $wydarzenie->miejsceWydarzenia->miasto;
            $ulica = $wydarzenie->miejsceWydarzenia->ulica;
            $js .= "\n{date: '$data', godzina: '$godzina', title: '$nazwa', miejsce: '$miasto, $ulica'},";
        }
        return $js;
    }
    
    public function zapiszMiejsce($miejsce) {
        return $this->miejsceWydarzeniaDao->zapiszMiejsce($miejsce);
    }
    
    public function wyszukajIstniejaceMiejsca($wzorzec) {
        $kryteria = new KryteriaWyszukiwaniaMiejsc();
        $kryteria->wzorzec = $wzorzec;
        $kryteria->ilosc = 4;
        return $this->miejsceWydarzeniaDao->wyszukajIstniejaceMiejsca($kryteria);
    }
    
    public function wyszukajWydarzenia($kryteria) {
        return $this->miejsceWydarzeniaDao->wyszukajIstniejaceMiejsca($kryteria);
    }
}
?>