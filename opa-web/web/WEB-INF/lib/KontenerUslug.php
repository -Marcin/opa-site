<?php
class KontenerUslug {
    private static $wydarzeniaUsluga;
    private static $authDao;
    
    private function __construct() {
    }
    
    public static function getWydarzenia() {
        if (self::$wydarzeniaUsluga == NULL) {
            self::$wydarzeniaUsluga = new WydarzeniaUsluga();
        }
        return self::$wydarzeniaUsluga;
    }
    
    public static function getAuth() {
        if (self::$authDao == NULL) {
            self::$authDao = new AuthDao();
        }
        return self::$authDao;
    }
}
?>
