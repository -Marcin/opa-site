<?php
//include_once 'lib/incl.php';

class Adres {
    public $id;
    public $ulica;
    public $miasto;
    public $nrBudynku;
    
    function __construct($ulica, $miasto, $nrBudynku) {
        $this->ulica = $ulica;
        $this->miasto = $miasto;
        $this->nrBudynku = $nrBudynku;
    }
}

class WynikZapisu {
    public $id;
    public $status;
    public $problem;
    public $info;
}

class StatusLogowania {
    public $login;
    public $status;
    public $opis;
}
?>