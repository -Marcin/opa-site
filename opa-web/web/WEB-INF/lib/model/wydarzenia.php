<?php
//include_once '../lib/incl.php';

class MiejsceWydarzenia {
    public $id;
    public $nazwa;
    public $miasto;
    public $ulica;
}

class Wydarzenie {
    public $id;
    public $dataCzas;
    public $miejsceWydarzenia;
    
    public function __construct() {
        $this->miejsceWydarzenia = new MiejsceWydarzenia();
    }
}

class KryteriaWyszukiwaniaWydarzen {
    public $id;
    public $data;
    public $dataDo;
    public $dataOd;
    public $miasto;
    public $miesiac;
    public $ilosc;
    public $orderBy;
}

class KryteriaWyszukiwaniaMiejsc {
    public $id;
    public $miasto;
    public $wzorzec;
    public $ilosc;
    public $orderBy;
}
?>