<?php
include_once (__DIR__ . '/../incl.php');

function login($p1, $p2) {
    $usr = $p1;
    $pass = md5(":$p1:").md5("$p2:");
    if (KontenerUslug::getAuth()->checkCredentials($usr, $pass)) {
        $tk = KontenerUslug::getAuth()->generujToken($usr);
        $_SESSION['opa-tk'] = $tk;
        setcookie('opa-tk', $tk);//$_COOKIE['opa-tk'] = $tk;
        $status = new StatusLogowania();
        $status->login = $usr;
        $status->status = true;
        $status->opis = $tk;
        return $status;
    } else {
        unset($_SESSION['opa-tk']);
        $status = new StatusLogowania();
        $status->login = $usr;
        $status->status = false;
        $status->opis = "Niepoprawne dane logowania";
        return $status;
    }
}

function checkSession() {
    //jezeli istnieje sesja
    if (isset($_SESSION['opa-tk']) && $_SESSION['opa-tk'] === $_COOKIE['opa-tk']) {
        return true;
    }
    if (isset($_COOKIE['opa-tk']) && $_COOKIE['opa-tk'] !== ""
            && KontenerUslug::getAuth()->sprawdzCzas($_COOKIE['opa-tk'])) {
        $_SESSION['opa-tk'] = $_COOKIE['opa-tk'];
        return true;
    }
    return false;
}
?>
