<?php
include_once (__DIR__ . '/../incl.php');
switch ($_GET['a']) {
    case '7a617069737a': zapiszWydarzenie(); break; //zapis
    case '65647974756a': break; //edycja
    case '7573756e': break;     //usuwanie
    default: break;
}
function zapiszWydarzenie() {
    $miejsce = new MiejsceWydarzenia();
    if ($_POST['idMiejsca']) {
        $miejsce->id = $_POST['idMiejsca'];
    } else {
        if ($_POST['nazwa'] === NULL || $_POST['miasto'] === NULL
                || $_POST['nazwa'] === "" || $_POST['miasto'] === "") {
            $wynik = new WynikZapisu();
            $wynik->status = false;
            $wynik->problem = "Brak miejsca";
            echo json_encode($wynik);
            return;
        }
        $miejsce->miasto = $_POST['miasto'];
        $miejsce->nazwa = $_POST['nazwa'];
        $miejsce->ulica = $_POST['ulica'];
    }
    $wydarzenie = new Wydarzenie();
    $wydarzenie->miejsceWydarzenia = $miejsce;
    
    if ($_POST['dataCzas'] === NULL || $_POST['dataCzas'] === "") {
            $wynik = new WynikZapisu();
            $wynik->status = false;
            $wynik->problem = "Brak daty";
            echo json_encode($wynik);
            return;
    }
    $wydarzenie->dataCzas = $_POST['dataCzas'];
    
    echo json_encode(@KontenerUslug::getWydarzenia()->zapiszWydarzenie($wydarzenie));
}

function edytujWydarzenie() {
    
}

function usunWydarzenie() {
    
}

?>
