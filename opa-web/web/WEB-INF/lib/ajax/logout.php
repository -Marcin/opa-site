<?php
session_start();
include_once(__DIR__ . '/../auth/auth.php');
$tk = isset($_SESSION['opa-tk']) ? $_SESSION['opa-tk'] : (isset($_COOKIE['opa-tk']) ? $_COOKIE['opa-tk'] : NULL);
if (isset($tk)) {
    KontenerUslug::getAuth()->logout($tk);
    setcookie('opa-tk', NULL, -1);
    $_SESSION['opa-tk'] = NULL;
}
session_destroy();
?>
