<?php
include_once (__DIR__ . '/../incl.php');

$wydarzenia = KontenerUslug::getWydarzenia();

$kryteria = new KryteriaWyszukiwaniaWydarzen();
$kryteria->data = $_GET['data'];
$kryteria->dataDo = $_GET['dataDo'];
$kryteria->dataOd = $_GET['dataOd'];
$kryteria->id = $_GET['id'];
$kryteria->miasto = $_GET['miasto'];
$kryteria->miesiac = $_GET['miesiac'];
$kryteria->orderBy = $_GET['orderBy'];
$kryteria->ilosc = $_GET['ilosc'];

echo json_encode(@$wydarzenia->wyszukajWydarzenia($kryteria));
?>
