<?php

//include_once 'lib/incl.php';

class WydarzenieDao extends AbstractDAO {
    public function __construct() {
       parent::__construct();
    }
    public function zapiszWydarzenie($wydarzenie) {
        
        $sql = "INSERT INTO wydarzenie (dataczas, id_miejsca) VALUES (:dataczas, :id_miejsca)";
        $stm = $this->daoManager->createQuery($sql);
        $stm->bindParam(":dataczas", $wydarzenie->dataCzas);
        $stm->bindParam(":id_miejsca", $wydarzenie->miejsceWydarzenia->id);
        $stm->execute();
        
        $id = $this->daoManager->getLastId();
        $wynik = new WynikZapisu();
        if ($id) {
            $wynik->id = $id;
            $wynik->status = true;
            $wynik->info = "Pomyślnie zapisano wydarzenie";
        } else {
            $wynik->status = false;
            $wynik->problem = "Nie można zapisac wydarzenia";
        }
            
        return $wynik;
    }

    public function edytujWydarzenie($wydarzenie) {
        $sql = "UPDATE wydarzenie SET dataczas = :data_czas, id_miejsca = :id_miejsca WHERE id_wydarzenia = :id_wydarzenia";

        $this->daoManager->createQuery($sql);
        $this->daoManager->bindParam(":dataczas", $wydarzenie->dataCzas);
        $this->daoManager->bindParam(":id_miejsca", $wydarzenie->miejsceWydarzenia->id);
        $this->daoManager->bindParam(":id_wydarzenia", $wydarzenie->id);
        $this->daoManager->execute();
    }

    public function usunWydarzenie($id) {
        $sql = "DELETE FROM wydarzenie WHERE id_wydarzenia = :id_wydarzenia";

        $this->daoManager->createQuery($sql);
        $this->daoManager->bindParam(":id_wydarzenia", $id);
        return $this->daoManager->execute() > 0;
    }

    public function pobierzWydarzenie($id) {
        
    }

    public function pobierzListeWydarzen($kryteria) {
        $sql = "SELECT DISTINCT w.id_wydarzenia AS id_wydarzenia, w.dataczas AS dataczas, 
            \nm.id_miejsca AS m_id_miejsca, m.nazwa AS m_nazwa, m.miasto AS m_miasto, m.ulica AS m_ulica 
            \nFROM wydarzenie w 
            \nJOIN miejsce m ON w.id_miejsca = m.id_miejsca WHERE (1=1)";
        
        if ($kryteria->dataDo) {
            $sql .= "\nAND w.dataczas <= :dataDo";
            $parametry[":dataDo"] = date("Y-m-d H:i", $kryteria->dataDo);
        }
        if ($kryteria->dataOd) {
            $sql .= "\nAND w.dataczas >= :dataOd";
            $parametry[":dataOd"] = date("Y-m-d H:i", $kryteria->dataOd);
        }
        if ($kryteria->miesiac) {
            $sql .= "\nAND YEAR(w.dataczas) = YEAR(:miesiac)"
                ."\nAND MONTH(w.dataczas) = MONTH(:miesiac)";
            $parametry[":miesiac"] = date("Y-m-d H:i", $kryteria->miesiac);
        }
        if ($kryteria->miasto) {
            $sql .="\nAND m.miasto = :miasto";
            $parametry[":miasto"] = $kryteria->miasto;
        }
        
        if ($kryteria->orderBy) {
            $sql .= "\nORDER BY $kryteria->orderBy";
        }
        if ($kryteria->ilosc) {
            $sql .= "\nLIMIT $kryteria->ilosc";
        }
        
        $query = $this->daoManager->createQuery($sql);
        
        foreach ($parametry as $key => $value) {
            $query->bindParam($key, $value);
        }
        $result = $query->fetchAll();
        if (NULL === $result) {
            return;
        }
        foreach ($result as $rs) {
            $wydarzenie = new Wydarzenie();
            $wydarzenie->id = $rs["id_wydarzenia"];
            $wydarzenie->dataCzas = strtotime($rs["dataczas"]);
            $wydarzenie->miejsceWydarzenia->id = $rs["m_id_miejsca"];
            $wydarzenie->miejsceWydarzenia->nazwa = $rs["m_nazwa"];
            $wydarzenie->miejsceWydarzenia->miasto = $rs["m_miasto"];
            $wydarzenie->miejsceWydarzenia->ulica = $rs["m_ulica"];
            $wynik[] = $wydarzenie;
            
        }
        return $wynik;
    }

}

class MiejsceWydarzeniaDao extends AbstractDAO {
    public function __construct() {
       parent::__construct();
    }
    public function zapiszMiejsce($miejsce) {
        $sql = "INSERT INTO miejsce (nazwa, miasto, ulica) VALUES (:nazwa, :miasto, :ulica)";
        
        $stm = $this->daoManager->createQuery($sql);
        $stm->bindParam(":nazwa", $miejsce->nazwa);
        $stm->bindParam(":miasto", $miejsce->miasto);
        $stm->bindParam(":ulica", $miejsce->ulica);
        $stm->execute();
        return $this->daoManager->getLastId("miejsce.id_miejsca");
    }

    public function edytujMiejsce($miejsce) {
        
    }

    public function usunMiejsce($id) {
        
    }

    public function pobierzMiejsce($id) {
        
    }
    
    public function wyszukajIstniejaceMiejsca($kryteria) {
        $sql = "SELECT DISTINCT id_miejsca, nazwa, miasto, ulica FROM miejsce WHERE (1=1)";
        
        if ($kryteria->miasto) {
            $sql .= "\nAND miasto = :miasto";
            $parametry[":miasto"] = $kryteria->miasto;
        }
        if ($kryteria->id) {
            $sql .= "\nAND id_miejsca = :id";
            $parametry[":id"] = $kryteria->id;
        }
        
        if ($kryteria->wzorzec) {
            $sql .= "\nAND ("
                    . "\nmiasto LIKE :wzorzec"
                    . "\nOR ulica LIKE :wzorzec"
                    . "\nOR nazwa LIKE :wzorzec)";
            $parametry[":wzorzec"] = "%$kryteria->wzorzec%";
        }
        
        if ($kryteria->orderBy) {
            $sql .= "\nORDER BY $kryteria->orderBy";
        }
        if ($kryteria->ilosc) {
            $sql .= "\nLIMIT $kryteria->ilosc";
        }
        
        $query = $this->daoManager->createQuery($sql);
        
        foreach ($parametry as $key => $value) {
            $query->bindParam($key, $value);
        }
        
        foreach ($query->fetchAll() as $rs) {
            $miejsce = new MiejsceWydarzenia();
            $miejsce->id = $rs["id_miejsca"];
            $miejsce->nazwa = $rs["nazwa"];
            $miejsce->miasto = $rs["miasto"];
            $miejsce->ulica = $rs["ulica"];
            $wynik[] = $miejsce;
        }
        return $wynik;
    }
}

?>