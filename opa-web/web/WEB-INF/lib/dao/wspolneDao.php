<?php
class PreparedStatement {
    public $stm;
    public $query;
    
    function __construct(&$stm, $query) {
        error_log($query);
        $this->stm = $stm;
        $this->query = $query;
    }
    
    public function fetch() {
        $this->stm->execute();
        return $this->stm->fetch();
    }
    public function fetchAll() {
        $this->stm->execute();
        return $this->stm->fetchAll();
    }
    
    public function bindParam($name, $value) {
        $this->stm->bindParam($name, $value);
        return $this;
    }
    
    public function execute() {
        return $this->stm->execute();
    }
}

class DaoManager {

    private static $instance;
    private static $conn;
    private static $initialized = false;

    private function __construct() {
        self::initialize();
    }

    private static function initialize() {
        $dbProperties = parse_ini_file(__DIR__.'/../../../../conf/db.ini');
        $servername = $dbProperties['host'];
        $dbname = $dbProperties['dbname'];
        $username = $dbProperties['user'];
        $password = $dbProperties['pass'];

        try {
            self::$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password,
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'", PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (PDOException $e) {
            echo $e->getMessage();
            throw new RuntimeException("Bład połączenia z bazą");
        }
        self::$initialized = true;
    }

    public static function getInstance() {
        if (self::$initialized) {
            return self::$instance;
        }
        self::$instance = new DaoManager();
        return self::$instance;
    }

    public function execute($sql) {
        self::$conn->exec($sql);
        return $this;
    }

    public function fetch() {
        return self::$conn->execute()->fetch();
    }

    public function createQuery($sql) {
        $stm = new PreparedStatement(self::$conn->prepare($sql), $sql);
        return $stm;
    }


    public function getLastId() {
        return self::$conn->lastInsertId();
    }
}

abstract class AbstractDAO {

    protected $daoManager;

    protected function __construct() {
        $this->daoManager = DaoManager::getInstance();
    }

}

?>