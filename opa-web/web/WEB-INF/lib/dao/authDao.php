<?php
 include_once(__DIR__.'/../auth/auth.php');
 
class AuthDao extends AbstractDAO {
    
    public function __construct() {
       parent::__construct();
    }
    
    public function checkCredentials($user, $pass) {
        $sql = "SELECT COUNT(1) AS ile FROM au WHERE us = :us && hsh = :hsh";
        
        $query = $this->daoManager->createQuery($sql);
        $query->bindParam(":us", $user);
        $query->bindParam(":hsh", $pass);
        
        $result = $query->fetch();
        if (NULL === $result) {
            return;
        }
        return $result["ile"] === "1";
    }
    
    public function generujToken($user) {
        $sql = "SELECT DISTINCT id FROM au WHERE us = :us";
        $query = $this->daoManager->createQuery($sql);
        $query->bindParam(":us", $user);
        $result = $query->fetchAll();
        if (NULL === $result) {
            throw new Exception("Zły użytkownik");
        }
        foreach ($result as $rs) {
            $id = $rs["id"];
        }
        if (!isset($id)) {
            throw new Exception("Zły użytkownik");
        }
        $tk_uq = uniqid();
        $sql = "INSERT INTO au_tk (id_au, tk) VALUES (:id, :tk)";
        $query = $this->daoManager->createQuery($sql);
        $query->bindParam(":id", $id);
        $query->bindParam(":tk", $tk_uq);
        do { //tk jest UQ, jeżeli już istnieje generuj nowy aż do skutku
            try {
                $result = $query->execute();
            } catch (PDOException $ex) {
                if (preg_match("(Duplicate entry).*(for key).*(tk_uq)", $ex->getMessage()) > 0) {
                    $tk_uq = uniqid();
                    continue;
                }
                throw $ex;
            }
            break;
        } while (true);
        
        return $tk_uq;
    }
    
    public function sprawdzCzas($tk) {
        $sql = "SELECT DISTINCT id, dt FROM au_tk WHERE tk = :tk";
        $query = $this->daoManager->createQuery($sql);
        $query->bindParam(":tk", $tk);
        $result = $query->fetch();
        if (NULL === $result) {
            return false;
        }
        if (strtotime($result['dt']) > (time() - 1000 * 60 * 60 * 24)) {
            $sql = "UPDATE au_tk SET dt = CURRENT_TIME() WHERE id = :id";
            $query = $this->daoManager->createQuery($sql);
            $query->bindParam(":id", $result['id']);
            $query->execute();
            return true;
        } else {
            $this->logout($tk);
            return false;
        }
        
    }

    public function logout($tk) {
        $sql = "DELETE FROM au_tk WHERE id = :tk";
        $query = $this->daoManager->createQuery($sql);
        $query->bindParam(":tk", $tk);
        $query->execute();
    }

}
?>
