<?php
include_once(__DIR__.'/../lib/incl.php');

$wydarzenia = KontenerUslug::getWydarzenia();
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>OPA!</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="OPA - world music, strona zespołu OPA">
        <meta name="keywords" content="opa, world, music, folk, improvisation, band">
        <link rel="stylesheet" href="../css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css/opa-theme.css" />
        <link rel="stylesheet" href="../css/font-awesome.min.css" />
        <link rel="stylesheet" href="../css/colorbox.css" />
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
        
        
        <script type="text/javascript" src="../js/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="../js/underscore-min.js"></script>
        <script type="text/javascript" src="../js/moment-2.5.1.js"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="../js/jquery.colorbox-pl.js"></script>
        <script type="text/javascript" src="../js/clndr.js"></script>
        <script type="text/javascript" src="../js/cookies.js"></script>
        <script type="text/javascript" src="../js/main.js" id="main-js"></script>
                
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-55425591-1', 'auto');
            ga('require', 'displayfeatures');
            ga('send', 'pageview');
        </script>

    </head>
    <body>
        <div id="stand-by"><div class="container">
                <h1>Ładowanie strony...</h1><i class="fa fa fa-spinner fa-spin fa-2x">&nbsp;</i><h3><small>Zbyt długo to trwa? <a href="#" onclick="wylaczEkranLadowania();">Kontynuuj...</a></small></h3>
            </div>
        </div>
        <div id="wrapper" style="display: none">
            <div class="top-bar">
                <a data-toggle="tooltip" data-original-title="opa.worldmusic@gmail.com" data-placement="bottom"  href="mailto:opa.worldmusic@gmail.com"><i class="fa fa-envelope">&nbsp;</i></a>
                <a data-toggle="tooltip" data-original-title="+48&nbsp;515355965" data-placement="bottom" href="#kontakt"><i class="fa fa-phone-square">&nbsp;</i></a>
                <a data-toggle="tooltip" data-original-title="fb.com/opa.worldmusic" data-placement="bottom"  href="https://facebook.com/opa.worldmusic/"><i class="fa fa-facebook-square">&nbsp;</i></a>
                <a data-toggle="tooltip" data-original-title="opaworldmusic.bandcamp.com" data-placement="bottom"  href="http://opaworldmusic.bandcamp.com/"><i class="fa fa-music">&nbsp;</i></a>
            </div>
            <div class="container" id="tiles">
                <div class="pre-tiles"><div class="opa-logo">&nbsp;</div><hr/></div>
                <div class="row center-block" id="main-tiles">
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="row">
                            <div class="tile col-md-6" id="onas">
                                <a href="#onas-header" onclick="pokazTresc();">O nas</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="tile col-md-6" id="opa1">
                                <div class="opa-image-tiles">
                                    &nbsp;
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="row">
                            <div class="tile col-md-6" id="opa2">
                                <div class="opa-image-tiles">
                                    &nbsp;
                                </div>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="tile col-md-6" id="galeria">
                                <a href="#galeria-header" onclick="pokazTresc();">Galeria</a>
                            </div> 
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4 col-xs-6"> 
                        <div class="row">
                            <div class="tile col-md-4" id="opa3">
                                <div class="opa-image-tiles">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="tile col-md-4" id="kontakt">
                                <a href="#kontakt-header" onclick="pokazTresc();">Kontakt</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="tile col-md-4" id="opa4">
                                <div class="opa-image-tiles">
                                    &nbsp;
                                </div></div> 
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-12 col-xs-6">
                        <div class="row">
                            <div class="tile col-md-12" id="kalendarz">
                                <!--Kalendarz-->
                                <h1><small style="color: #fff">
                                        <a href="#kalendarz-header" onclick="pokazTresc();">Wydarzenia</a></small></h1>
                                <div class="table-responsive"><table class="table table-condensed"><thead>
                                            <tr><th>DATA</th><th>GODZINA</th><th>LOKALIZACJA</th></thead>
                                        <tbody>
                                            <?php
                                            $listaWydarzen = @$wydarzenia->pobierzNajblizszeWydarzenia(3);
                                            if (NULL !== $listaWydarzen) {
                                                foreach ($listaWydarzen as $wydarzenie) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo date('Y-m-d', $wydarzenie->dataCzas); ?></td>
                                                        <td><?php echo date('H:i', $wydarzenie->dataCzas); ?></td>
                                                        <td><?php echo $wydarzenie->miejsceWydarzenia->miasto ?></td>
                                                    </tr>
                                                <?php }
                                            }
                                            ?>
                                            <tr>
                                                <td>??</td><td>??</td><td>???</td>
                                            </tr>
                                        </tbody>
                                    </table></div>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="post-tiles"><hr/></div>
            </div>
            <div class="container" id="przelacznik-tresci">
                <div id="pokaz-tresc">
                    <a href="#" onclick="pokazTresc();"><i class="fa fa-circle fa-2x fa-stack-2x" style="color: #333"></i>
                        <i class="fa fa-angle-double-down fa-4x fa-stack-2x fa-inverse"></i></a>
                </div>
                <div id="ukryj-tresc" style="display: none">
                    <a href="#" onclick="ukryjTresc();"><i class="fa fa-circle fa-2x fa-stack-2x" style="color: #333"></i>
                        <i class="fa fa-angle-double-up fa-4x fa-stack-2x fa-inverse"></i></a>
                </div>
            </div>
        </div>

        <div class="container" id="tresc" style="display: none">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container-fluid " id="navbar-top">
                    <div class="navbar-collapse">
                        <div class="row nav navbar-nav">
                            <h2>
                                <a href="#onas-header"><span class="label label-onas">O nas</span></a>
                                <a href="#galeria-header"><span class="label label-galeria">Galeria</span></a>
                                <a href="#kontakt-header"><span class="label label-kontakt">Kontakt</span></a>
                                <a href="#kalendarz-header"><span class="label label-kalendarz">Kalendarz</span></a>
                            </h2>
                        </div>
                    </div>                    
                </div>
            </nav>
            <div class="center-block" id="main-content">
                <div class="page-header" id="onas-header"><h2><span class="label label-onas">O nas</span></h2></div>

                <div class="row" id="onas-content">
                    <div class="col-md-8 col-sm-9">
                        <p><em>OPA!</em> to żywioł autorskich aranżacji tradycyjnych pieśni.
                            Nie gramy konkretnego gatunku - muzyka ma dawać wolność, a nie ustawiać w szeregu.
                            Nie ograniczamy się do jednego regionu świata - muzyka nie ma granic.</p>

                        <p>Czerpiemy inspiracje z tradycyjnych pieśni cygańskich, śląskich, ukraińskich, tureckich, żydowskich... właściwie nie ograniczamy się do konkretnego miejsca na Ziemi.</p>

                        <p>Śpiewamy o miłości, tęsknocie, synowej, o ojcu, który nie chciał kupić córce sukienki, o mężu, który bawi się, w trakcie kiedy jego żona pierze mu koszule.</p>

                        <p>Jest nas czworo. Każdy inny, ale każdy stanowi integralną część zespołu.
                            Kuba to główny aranżer, gitarzysta. 
                            Ewa to skrzypaczka, na co dzień chórzystka częstochowskiej filharmonii. Oprócz grania również śpiewa. 
                            Marcin to basista, ostatnio śpiewający. 
                            Iwona to nasza wokalistka.</p>

                        <p>Początki zespołu związane są z Kubą, Ewą i Marcinem. Muzykowali ze sobą dopóki Ewa nie wyjechała na wolontariat europejski do Gruzji.<br />
                            W lutym 2014 roku na warsztatach pieśni miłosnych w Ośrodku Edukacji Kulturowej Kuba poznał naszą wokalistkę.<br />
                            Przez kilka miesięcy byli w trójkę, po powrocie Ewy występują już w pełnym składzie.</p>

                        <h4>Tam występowaliśmy:</h4>
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-check-square"></i>Festiwal Zderzenie Gatunków</li>
                            <li><i class="fa-li fa fa-check-square"></i>Najmniejszy Koncert w Tychach</li>
                            <li><i class="fa-li fa fa-check-square"></i>Jarmark produktów tradycyjnych</li>
                            <li><i class="fa-li fa fa-check-square"></i>Jarmark św. Marcina</li>
                            <li><i class="fa-li fa fa-check-square"></i>Rudy Goblin</li>
                        </ul>
                    </div><div class="col-xs-4"><a href="../images/max/wsp1.jpg" class="img-lightbox"><img src="../images/min/wsp1.jpg" style="width: 300px;"/></a></div>
                </div>
                <hr/>
                <div class="page-header" id="galeria-header"><h2><span class="label label-galeria">Galeria</span></h2></div>
                <div class="row" id="galeria-content">
                    <div class="col-xs-6" id="galeria-content-zdjecia">
                        <div class="row">
                            <h3 style="text-align: left"><small><i class="fa fa-photo">&nbsp;</i>Zdjęcia
                                    <a href="https://www.facebook.com/opa.worldmusic/photos_stream">
                                        <span class="label label-default">Więcej</span>
                                    </a>
                                </small></h3>
                            <div class="col-md-6 col-xs-12">
                                <a href="../images/max/don-martinez.jpg" class="thumbnail img-lightbox"><img src="../images/min/don-martinez.jpg"/></a>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <a href="../images/max/krakow.jpg" class="thumbnail img-lightbox"><img src="../images/min/krakow.jpg"/></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6" id="galeria-content-audio">
                        <div class="row">
                            <h3 style="text-align: left"><small><i class="fa fa-music">&nbsp;</i>Multimedia
                                    <a href="http://opaworldmusic.bandcamp.com/">
                                        <span class="label label-default">Więcej</span>
                                    </a>
                                </small></h3>

                            <div class="col-md-6 col-xs-12">
                                <div class="thumbnail">
                                    <div class="caption"><h4>Sinoć Sjala</h4>
                                        <h2><a href="http://opaworldmusic.bandcamp.com/track/sino-sjala" data-bc="8693560" class="bc-lightbox label label-success">Posłuchaj</a></h2>
                                    </div>
                                    <img src="../images/play-audio.png" alt="Sinoć sjala" title="Sinoć sjala"/>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <div class="thumbnail">
                                    <div class="caption"><h4>Andre Tire</h4>
                                        <h2><a href="https://www.youtube.com/watch?v=MIUcl-39jSU" class="yt-lightbox label label-success">Obejrzyj</a></h2>
                                    </div>
                                        <img src="../images/play-video.png" alt="Andre Tire" title="Andre Tire"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                        <hr/>
                        <div class="page-header" id="kontakt-header"><h2><span class="label label-kontakt">Kontakt</span></h2></div>
                        <div class="row" id="kontakt-content">
                            <div class="row">
                                <div class="col-xs-12"><ul class="fa-ul">
                                        <li><i class="fa-li fa fa-envelope-o"></i><small>Email:</small> <a href="mailto:opa.worldmusic@gmail.com">opa.worldmusic@gmail.com</a></li>
                                        <li><i class="fa-li fa fa-phone"></i><small>Telefon:</small> +48 515355965</li>
                                        <li><i class="fa-li fa fa-facebook"></i><small>Facebook:</small> <a href="https://facebook.com/opa.worldmusic/">fb.com/opa.worldmusic</a></li>
                                        <li><i class="fa-li fa fa-external-link"></i><small>Bandcamp:</small> <a href="http://opaworldmusic.bandcamp.com/">opawordmusic</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="page-header" id="kalendarz-header"><h2><span class="label label-kalendarz">Kalendarz</span></h2></div>
                        <div class="row" id="kalendarz-content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4" id="kalendarz-box">
                                    <script type="text/template" id="kalendarz-template">
                                        <% var it = 1;%>
                                        <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                        <th class="clndr-previous-button" colspan="2"><button class="btn-kalendarz">&lsaquo;</button></th>
                                        <th class="month" colspan="3"><%= tlumaczMiesiac(month) %>&nbsp;<%= year %></th>
                                        <th class="clndr-next-button" colspan="2"><button class="btn-kalendarz">&rsaquo;</button></th>
                                        </tr>
                                        <tr>
                                        <% _.each(daysOfTheWeek, function(day) { %>
                                        <th><%= day %></th>
                                        <% }); %>
                                        </tr>
                                        </thead>
                                        <tbody id="kalendarz-dni">
                                        <tr>
                                        <td class="empty last-month">&nbsp;</td>
                                        <td class="empty last-month">&nbsp;</td>
                                        <td class="empty last-month">&nbsp;</td>
                                        <td class="empty last-month">&nbsp;</td>
                                        <td class="empty last-month">&nbsp;</td>
                                        <td class="empty last-month">&nbsp;</td>
                                        <% _.each(days, function(day) { %>
                                        <!--<% if (it % 7  === 0) { %>
                                        <tr id="tydzien-<%= it/7 %>">
                                        <% } %>-->
                                        <td class="<%= day.classes %>"><button class="btn-kalendarz"><%= day.day %></button></td>
                                        <% if (it % 7 === 1) { %>
                                        </tr>
                                        <% } 
                                        it++;%>
                                        <% }); %>
                                        <td class="empty last-month">&nbsp;</td></tr>
                                        </tbody>
                                        </table>                                
                                    </script>          
                                    <script type="text/javascript" id="generujKalendarz">
                                        var events = [
                                        <?php
                                        echo @$wydarzenia->generujJS(@$wydarzenia->pobierzWydarzeniaMiesiaca(time()));
                                        ?>
                                        ];
                                    
                                    
//                                            {date: '2014-10-11', godzina: '12:00', title: 'Jarmark Produktów Tradycyjnych', miejsce: 'Kraków, Mały Rynek'},
//                                            {date: '2014-10-12', godzina: '14:00', title: 'Jarmark Produktów Tradycyjnych', miejsce: 'Kraków, Główny Rynek'},
                                        generujKalendarz(events);
                                    </script>

                                    <a href="#" data-target="#dodaj-wydarzenie" id="modal-on-dodaj-wydarzenie" data-toggle="modal" style="display: none">dodaj-wydarzenie</a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-sm-offset-2">
                                    <div class="well well-lg">
                                        <h3>Najbliższe wydarzenia:</h3><hr/>
                                        <?php
                                        $listaWydarzen = @$wydarzenia->pobierzNajblizszeWydarzenia(3);
                                        if (NULL === $listaWydarzen) {
                                            echo "Brak nadchodących wydarzeń!";
                                        } else {
                                            foreach ($listaWydarzen as $wydarzenie) {
                                                ?>
                                                <h4><?php echo date('d', $wydarzenie->dataCzas); ?> 
                                                    <script type="text/javascript">document.write(tlumaczMiesiac('<?php echo date('m', $wydarzenie->dataCzas); ?>'));</script>
                                                    <?php echo date('Y H:i', $wydarzenie->dataCzas); ?></h4>
                                                <p><?php
                                                    echo "<em>" . $wydarzenie->miejsceWydarzenia->nazwa . "</em>";
                                                    if ($wydarzenie->miejsceWydarzenia->ulica) {
                                                        echo "<small>";
                                                        echo "<br />" . $wydarzenie->miejsceWydarzenia->ulica;
                                                        echo "</small>";
                                                    }
                                                    echo ", " . $wydarzenie->miejsceWydarzenia->miasto;
                                                    echo "<hr/>";
                                                    ?>

    <?php }
}
?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <div class="modal" id="dodaj-wydarzenie" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header"><h3>Dodaj wydarzenie</h3></div>
                    <div class="modal-body">
                        <fieldset id="dodaj-wydarzenie-fset">
                            <form role="form" class="form-group" id="dodaj-wydarzenie-form">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <input type="hidden" id="noweMiejsce" name="noweMiejsce" value="false"/>
                                        <div class="input-group">
                                            <span class="input-group-addon active input-sm" id="sprawdz-miejsca">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" style="width: 100px" disabled="disabled" name="data" id="data" 
                                                   value="2014-12-10" class="form-control input-sm text-center">
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <span class="input-group-addon active input-sm" id="sprawdz-miejsca">
                                                <i class="fa fa-clock-o"></i>
                                            </span>
                                            <input class="form-control input-sm text-center required" type="text" value="12:00" id="godzina" 
                                                   name="godzina" disabled="disabled" style="width: 60px"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-5">
                                        <input class="form-control input-sm required" type="range" step="1" min="0" max="23" value="12:00"
                                               onchange="$('#godzina').attr('value', this.value + ':00');"/>
                                    </div>
                                </div>
                                <hr/>
                                <div class="center-block clearfix">
                                    <span id="miejsce-btn-istniejace" class="btn btn-sm btn-warning" onclick="przelaczDodanieMiejsca(false);">
                                        <i class="fa fa-search">&nbsp;</i>Istniejące miejsce</span>
                                    <span id="miejsce-btn-nowe" class="btn btn-sm btn-default" onclick="przelaczDodanieMiejsca(true);">
                                        <i class="fa fa-edit">&nbsp;</i>Nowe miejsce</span>
                                </div>
                                <div id="znajdz-miejsce" class="row">
                                    <div class="col-xs-2">
                                        <label for="miejsce" class="">Wyszukaj</label>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="input-group">
                                            <input id="miejsce" name="miejsce" type="text" list="miejsceDList" class="form-control input-sm required" 
                                                   placeholder="Nazwa, miasto, ulica..." oninput="wyszukajMiejsca(this.value);"/>
                                            <span id="sprawdz-miejsca" class="input-group-addon active">
                                                <i class="fa fa-check" onclick="sprawdzMiejsca();"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <datalist id="miejsceDList"></datalist>
                                    <div class="col-xs-3">

                                    </div>
                                    <input type="hidden" id="id-miejsca" name="id-miejsca" default=""/>
                                </div>
                                <div id="dodaj-miejsce" style="display: none">
                                    <div class="row">
                                        <div class="col-xs-2"><label for="nazwa">Nazwa</label></div>
                                        <div class="col-xs-6">
                                            <input class="form-control input-sm required" type="text" id="nazwa" name="nazwa" placeholder="Nazwa"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-2"><label for="miasto">Miasto</label></div>
                                        <div class="col-xs-6">
                                            <input class="form-control input-sm required" type="text" id="miasto" name="miasto" placeholder="Miasto"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-2"><label for="ulica">Ulica</label></div>
                                        <div class="col-xs-6">
                                            <input class="form-control input-sm" type="text" id="ulica" name="ulica" placeholder="Ulica (opcjonalne)"/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                            </form>
                        </fieldset>
                    </div>
                    <div class="footer row">
                        <div class="col-xs-6 col-xs-offset-3">
                            <div id="wynik-zapisu" class="alert alert-success" style="display: none;">
                                <i class="fa fa-check">&nbsp;</i>
                                <span class="success" id="wynik-zapisu-info"></span>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <span><a onclick="zamknijZapis()" data-dismiss="modal" id="zamknij" class="btn btn-success btn-sm" style="display: none;">Zamknij</a>
                                <a class="btn btn-primary btn-sm" id="zapisz" onclick="zapiszWydarzenie();">Zapisz</a>
                                <a class="btn btn-default btn-sm" id="anuluj" onclick="resetujDodanie();" data-dismiss="modal">Anuluj</a>
                            </span>
                        </div>
                    </div><br />
                </div>
            </div>
        </div>
                <div id="footer" style="display: none">
                    <footer class="container center-block">
                        <div class="row">
                            <div class="col-xs-6">
                                <i class="fa fa-lock">&nbsp;</i> OPA@2014v0.3
                            </div>
                            <div class="col-xs-4 col-xs-offset-2">
                                Strona w budowie, 
                                <a href="https://bitbucket.org/-Marcin/opa-site/issues">zgłoś błąd</a>
                            </div>
                        </div>
                    </footer>
                </div> 
                <!--        <div id="side-fb">FACEBOOK</div>
                                <div id="side-fb-content" style="position: fixed; top: 100px; left: 100px; width: 400px; height: 300px; float: left;">
                                    <div class="fb-like-box" 
                                         data-href="https://www.facebook.com/opa.worldmusic" 
                                         data-colorscheme="light" data-show-faces="true" 
                                         data-header="true" data-stream="false" 
                                         data-show-border="true">
                                    </div>
                
                
                                </div>-->
                </body>
</html>
