
function  przelaczDodanieMiejsca(bool) {
    if (bool) { //dodaj nowe
        $("#znajdz-miejsce").hide(); //ukryj istniejace
        $("#dodaj-miejsce").show(); //pokaz nowe
        $("#noweMiejsce").val("true"); //set flaga
        $("span#miejsce-btn-nowe").removeClass("btn-default");
        $("span#miejsce-btn-nowe").addClass("btn-warning");
        $("span#miejsce-btn-istniejace").removeClass("btn-warning");
        $("span#miejsce-btn-istniejace").addClass("btn-default");
    } else {
        $("#dodaj-miejsce").hide(); //ukryj nowe
        $("#znajdz-miejsce").show(); //pokaz istniejace
        $("#noweMiejsce").val("false"); //set flaga
        $("span#miejsce-btn-nowe").removeClass("btn-warning");
        $("span#miejsce-btn-nowe").addClass("btn-default");
        $("span#miejsce-btn-istniejace").removeClass("btn-default");
        $("span#miejsce-btn-istniejace").addClass("btn-warning");
    }
}
function sprawdzMiejsca() {
    var $inputIdMiejsca = $("#dodaj-wydarzenie #id-miejsca");
    var $inputMiejsce = $("input#miejsce");
    var $btnDoZatwierdzenia = $("#sprawdz-miejsca i");
    $inputMiejsce.removeAttr("disabled");
    var $doZatwierdzenia = $btnDoZatwierdzenia.hasClass("fa-check");
    var $wartoscId = $inputIdMiejsca.val();
    if ($doZatwierdzenia) {
        if ($wartoscId > 0) {
            $inputMiejsce.parent().removeClass("has-error");
            $inputMiejsce.attr("disabled", "disabled");
            $btnDoZatwierdzenia.removeClass("fa-check");
            $btnDoZatwierdzenia.addClass("fa-close");
            $inputMiejsce.parent().addClass("has-success");
            $inputMiejsce.val($("#miejsceDList option#" + $wartoscId).val());
        } else {
            $inputMiejsce.parent().addClass("has-error");
        }
    } else {
        $inputMiejsce.parent().removeClass("has-error");
        $inputMiejsce.parent().removeClass("has-success");
        $inputMiejsce.removeAttr("disabled");
        $btnDoZatwierdzenia.removeClass("fa-close");
        $btnDoZatwierdzenia.addClass("fa-check");
        $inputMiejsce.attr("data-id-miejsca", "");
        $inputMiejsce.val("");
        $inputIdMiejsca.val("");
    }
}

function resetujDodanie() {
    $("span#miejsce-btn-nowe").removeClass("btn-warning");
    $("span#miejsce-btn-nowe").addClass("btn-default");
    $("span#miejsce-btn-istniejace").removeClass("btn-default");
    $("span#miejsce-btn-istniejace").addClass("btn-warning");
    $("#dodaj-miejsce").hide(); //ukryj nowe
    $("#znajdz-miejsce").show(); //pokaz istniejace
    var $inputMiejsce = $("input#miejsce");
    $inputMiejsce.parent().removeClass("has-error");
    $inputMiejsce.parent().removeClass("has-success");
    $inputMiejsce.removeAttr("disabled");
    var $btnDoZatwierdzenia = $("#sprawdz-miejsca i");
    $btnDoZatwierdzenia.removeClass("fa-close");
    $btnDoZatwierdzenia.addClass("fa-check");
    $("#dodaj-wydarzenie-form")[0].reset();
}

function wyszukajMiejsca(wzorzec) {
    if (wzorzec.trim().length < 3) {
        return;
    }

    var $wartosc = $("input#miejsce").val();
    if ($wartosc) {
        var $wybraniec = $("#miejsceDList option[value='" + $wartosc + "']");
        $("#dodaj-wydarzenie #id-miejsca").attr("value", $wybraniec.attr("id"));
        $("#dodaj-wydarzenie #miejsce").attr("data-id-miejsca", $wybraniec.attr("id")); 
        if ($wybraniec.attr("id")) {
            return;
        }
    }
    
    $.ajax("../lib/ajax/wyszukajMiejsca.php?wz=" + wzorzec, {
        dataType: 'json',
        success: function($miejsca) {
            zasilFormularzMiejsca($miejsca);
        },
    });

}


function zasilFormularzMiejsca($miejsca) {
  var $list = $("#miejsceDList");
  $("#miejsceDList option").remove();
  $($miejsca).each(function() {
      
      var nazwa = this["nazwa"].replace(/['`\"\t\n]/g);
      var ulica = null;
      var miasto = this["miasto"].replace(/['`\"\t\n]/g);
      var nazwaOpisowa = miasto + ", " + nazwa;
      if (this["ulica"]) {
          ulica = this["ulica"].replace(/['`\"\t\n]/g);
          nazwaOpisowa += ", " + ulica;
      }
      var $opt = $(
        "<option id='" + this["id"] + "' value='" + nazwaOpisowa + "' "
                + "data-miasto='" + miasto + "' "
                + "data-nazwa='" + nazwa + "' "
                + "data-ulica='" + ulica + "'>");
      $list.append($opt);
  });
}

function zapiszWydarzenie() {
    if (!walidujFromularz('dodaj-wydarzenie-form')) {
        return;
    }
        wydarzenie = {
            dataCzas: $("#dodaj-wydarzenie-form input[name='data']").val() 
                    + " " + $("#dodaj-wydarzenie-form input[name='godzina']").val(),
            idMiejsca: $("#dodaj-wydarzenie-form input[name='id-miejsca']").val(),
            nazwa:  $("#dodaj-wydarzenie-form input[name='nazwa']").val(),
            miasto:  $("#dodaj-wydarzenie-form input[name='miasto']").val(),
            ulica:  $("#dodaj-wydarzenie-form input[name='ulica']").val(),
        };
    $.ajax({
        url: '../lib/ajax/zapisWydarzen.php?a=7a617069737a',
        method: 'POST',
        dataType: 'json',
        data: wydarzenie,
        success: function(wynik) {
            if (wynik && wynik.status) {
                $("#dodaj-wydarzenie #zapisz").hide();
                $("#dodaj-wydarzenie #anuluj").hide();
                $("#dodaj-wydarzenie-fset").hide(2);
                $("#wynik-zapisu-info").html("Wydarzenie zostało dodane");
                $("#wynik-zapisu").show();
                $("#dodaj-wydarzenie #zamknij").show();
            }
        },
    });
}

function zamknijZapis() {
    resetujDodanie();
    $("#dodaj-wydarzenie #zapisz").show();
    $("#dodaj-wydarzenie #anuluj").show();
    $("#dodaj-wydarzenie-fset").show();
    $("#wynik-zapisu-info").html("");
    $("#wynik-zapisu").hide();
    $("#dodaj-wydarzenie #zamknij").hide();
}

function logout() {
    $.ajax({
        url: '../lib/ajax/logout.php',
        method: 'GET',
        success: function() {
            WHCreateCookie('opa-tk', '', -1);
            location.reload();
        },
    });
}
