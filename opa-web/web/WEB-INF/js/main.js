var kalendarz;
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); //aktywuj tooltipy
});
$(document).ready(function () {
    $('a[href*="http://"]').attr("target", "_BLANK");
    $('a[href*="https://"]').attr("target", "_BLANK");
         
     $('.yt-lightbox').colorbox({iframe: true, width: 640, height: 390, href:function(){
        var videoId = new RegExp('[\\?&]v=([^&#]*)').exec(this.href);
        if (videoId && videoId[1]) {
            return 'http://youtube.com/embed/'+videoId[1]+'?rel=0&wmode=transparent';
        }
    }});
    
    $('.img-lightbox').colorbox({
        maxWidth: "80%",
        maxHeight: "80%",
        innerHeight: "70%"    
    });
    
    $('.bc-lightbox').colorbox({html: function() { return '<iframe style="border: 0; width: 100%; height: 120px;" src="http://bandcamp.com/EmbeddedPlayer/track=' + this.getAttribute("data-bc") + '/size=large/bgcol=333333/linkcol=ffffff/tracklist=false/artwork=none/transparent=true/" seamless><a href="http://opaworldmusic.bandcamp.com/track/sino-sjala" target="_blank">Sinoć sjala od Opa!</a></iframe>'; }});

$('.thumbnail').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 
        
    
    
$(window).load(function() {
    wylaczEkranLadowania();
    $('#stand-by').remove();
    
		});
    
    //DEBUG--------------------------
    //pokazTresc();
});

function pokazTresc() {
    $('#tresc').slideDown();
    $('#footer').slideDown();    
    $('#ukryj-tresc').removeAttr("style");    
    
    $('#tiles').slideUp();
    $('#pokaz-tresc').css("display", "none");
}
function ukryjTresc() {
    $('#tiles').slideDown();
    $('#pokaz-tresc').removeAttr("style");   
    
    $('#tresc').slideUp();
    $('#footer').slideUp();
    $('#ukryj-tresc').css("display", "none");     
}

function wylaczEkranLadowania() {
    $('#stand-by').css("display", "none");
    $('#wrapper').removeAttr("style");
}

function powiekszObrazek(miniUrl) {
    var url = miniUrl.replace('/min/', '/max/');    
    $('#img-popup #lightbox-img').attr('src', url);
    $('#img-popup').lightbox();
}

function tlumaczMiesiac(month) {
    switch (month) {
        case '01': case 'January': return 'Styczeń';
        case '02': case 'February': return 'Luty';
        case '03': case 'March': return 'Marzec';
        case '04': case 'April': return 'Kwiecień';
        case '05': case 'May': return 'Maj';
        case '06': case 'June': return 'Czerwiec';
        case '07': case 'July': return 'Lipiec';
        case '08': case 'August': return 'Sierpień';
        case '09': case 'September': return 'Wrzesień';
        case '10': case 'October': return 'Październik';
        case '11': case 'November': return 'Listopad';
        case '12': case 'December': return 'Grudzień';
        default: return '--MIESIĄC--';
    }
}


function walidujFromularz(id) {
    var walidacja = true;
    $("#" + id + " input:visible, #" + id + " textarea:visible").each(function() {
        var $parent = $(this).parent();
        $parent.removeClass("has-error");
        if ($(this).hasClass("required") && ($(this).val() === null || $(this).val() === "")) {
            walidacja = false;
            $parent.addClass("has-error");
        }
    });
  return walidacja;
}

function generujKalendarz(events, clickEvents) {
    kalendarz = $('#kalendarz-box').clndr({
        template: $('#kalendarz-template').html(),
        showAdjacentMonths: false,
        adjacentDaysChangeMonth: false,
        forceSixRows: true,
        daysOfTheWeek: ["PN", "WT", "ŚR", "CZW", "PT", "SO", "ND"],
        events: events,
        clickEvents: clickEvents,
    });
    
    /*
     * {
            click: function(target) {
                if (target["date"] !== null) {
                    $("#dodaj-wydarzenie-form input[name='data']").val(target.date.format('YYYY-MM-DD'));
                    $("#modal-on-dodaj-wydarzenie").click();
                }
            },
            onMonthChange: function(month) {
                $.ajax("../lib/ajax/wyszukajWydarzenia.php?miesiac=" + month._d.getTime(), {
                    dataType: 'json',
                    success: function(wydarzenia) {
                        console.log("AJAX PUFFFF!");
                        console.log(wydarzenia);
                        kalendarz.clndr({
                            events: wydarzenia,
                        });
                    },
                });
            }
        }
     */
}

function zaloguj() {
    if (!walidujFromularz("zaloguj-form")) {
        return;
    }
    $form = $("#zaloguj-form");
    $("#loginError-div").hide();
    $form.hide();
    var dane = {
        kebab: $("#zaloguj-form #usr").val(),
        okon: sha256_digest($("#zaloguj-form #pass").val()),
    }
    $.ajax({
        url: '../lib/ajax/login.php',
        method: 'POST',
        dataType: 'json',
        data: dane,
        success: function(wynik) {
            if (wynik.status === true) {
                WHCreateCookie('opa-tk', wynik.opis, 1);
                $("#anuluj-btn").click();
                location.reload();
            } else {
                $("#loginError").html(wynik.opis);
                $("#loginError-div").stop();
                $("#loginError-div").show('slow');
                $form.show('fast');
            }
        },
    });
}

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 100
        }, 1000);
        return false;
      }
    }
  });
});